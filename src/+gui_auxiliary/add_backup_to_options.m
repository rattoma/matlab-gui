function options_ = add_backup_to_options(options_, backup)
%function backup_options()
% adds fields in backup to options_
%
% INPUTS
%    options_
%    backup
%
% OUTPUTS
%    options_
%
% SPECIAL REQUIREMENTS
%    none

% Copyright (C) 2019 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

fields = fieldnames(backup);
for i = 1:length(fields)
    options_.(fields{i}) = backup.(fields{i});
end
end
