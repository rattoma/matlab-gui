function backup = backup_options(options_)
%function backup = backup_options(options_)
% backs up those parts of options_ that would be overwritten 
% by a statement such as: options_ = default_option_values(M_);
%
% INPUTS
%    options_
%
% OUTPUTS
%    backup
%
% SPECIAL REQUIREMENTS
%    none

% Copyright (C) 2019 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

backup = struct();

if isfield(options_, 'varobs')
    backup.varobs = options_.varobs;
end

if isfield(options_, 'varobs_id')
    backup.varobs_id = options_.varobs_id;
end

if isfield(options_, 'DynareRandomStreams')
    backup.DynareRandomStreams = options_.DynareRandomStreams;
end

if isfield(options_, 'DynareRandomStreams')
    backup.DynareRandomStreams = options_.DynareRandomStreams;
end

if isfield(options_, 'bytecode')
    backup.bytecode = options_.bytecode;
end

if isfield(options_, 'block')
    backup.block = options_.block;
end

if isfield(options_, 'use_dll')
    backup.use_dll = options_.use_dll;
end

end
