function gui_observed_vars(tabId)
% function gui_observed_vars(tabId)
% interface for the observed variables and data file functionality
%
% INPUTS
%   tabId:      GUI tab element which displays the interface
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2003-2019 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global project_info model_settings options_

if ~isfield(model_settings, 'varobs')
    try
        model_settings.varobs = create_varobs_cell_array(evalin('base','options_.varobs'), evalin('base','M_.endo_names_tex'), evalin('base','M_.endo_names_long'), evalin('base','options_.varobs_id'));
        project_info.modified = true;
    catch ME
        model_settings.varobs = [];
    end
end

varobs = model_settings.varobs;

if ~isfield(model_settings, 'all_varobs')
    model_settings.all_varobs = {};
end
all_varobs = model_settings.all_varobs;

bg_color = char(getappdata(0,'bg_color'));
special_color = char(getappdata(0,'special_color'));

handles = [];

gui_size = gui_tools.get_gui_elements_size(tabId);

uicontrol(tabId,'Style','text',...
    'String','Observed variables & data file:',...
    'FontWeight', 'bold', ...
    'HorizontalAlignment', 'left','BackgroundColor', bg_color,...
    'Units','normalized','Position',[0.01 0.92 1 0.05] );

panel_id = uipanel( ...
    'Parent', tabId, 'Tag', 'uipanelSettings', ...
    'Units', 'normalized', 'Position', [0.01 0.09 0.98 0.82], ...
    'BackgroundColor', special_color, ...
    'Title', '', 'BorderType', 'none');

create_panel_elements(panel_id);

uicontrol(tabId, 'Style','pushbutton','String','Save changes','Units','normalized','Position',[gui_size.space gui_size.bottom gui_size.button_width gui_size.button_height], 'Callback',{@save_changes} );
uicontrol(tabId, 'Style','pushbutton','String','Select observed var.','Units','normalized','Position',[gui_size.space*2+gui_size.button_width gui_size.bottom gui_size.button_width gui_size.button_height], 'Callback',{@select_vars});
uicontrol(tabId, 'Style','pushbutton','String','Remove selected obs.vars','Units','normalized','Position',[gui_size.space*3+gui_size.button_width*2 gui_size.bottom gui_size.button_width gui_size.button_height], 'Callback',{@remove_selected});
uicontrol(tabId, 'Style','pushbutton','String','Close this tab','Units','normalized','Position',[gui_size.space*4+gui_size.button_width*3 gui_size.bottom gui_size.button_width gui_size.button_height], 'Callback',{@close_tab,tabId} );

    function save_changes(~, ~, ~)
        try
            if isempty(handles.first_obs.String)
                gui_tools.show_warning('The sample starting point is not specified');
            elseif isempty(handles.frequency.Value)
                gui_tools.show_warning('The data frequency is not specified');
            elseif isempty(handles.num_obs.String)
                gui_tools.show_warning('The number of observations is not specified');
            elseif isempty(handles.data_file.String)
                gui_tools.show_warning('The data file is not specified!');
            else
                project_info.first_obs = handles.first_obs.String;
                project_info.first_obs_date = dates(handles.first_obs.String);
                project_info.last_obs_date = dates(handles.first_obs.String) + str2double(handles.num_obs.String) - 1;
                project_info.freq = handles.frequency.Value;
                project_info.nobs = handles.num_obs.String;
                project_info.data_file = handles.data_file.String;
                project_info.modified = true;
                if project_info.new_data_format
                    %new data interface
                    options_.dataset.file = handles.data_file.String;
                    options_.dataset.firstobs = dates(handles.first_obs.String);
                    options_.dataset.nobs = str2double(handles.num_obs.String);
                else
                    % old data interface
                    options_.datafile = handles.data_file.String;
                    options_.nobs = str2double(handles.num_obs.String);
                end
                remove_selected();
                model_settings.varobs = varobs;
                if ~isempty(varobs)
                    options_.varobs = varobs(:, 1);
                end
                project_info.observed_variables_set = true;
                gui_set_menus(true);
            end
        catch ME
            gui_tools.show_error('Error saving observed variables and data file information', ME, 'extended');
        end
    end

    function close_tab(~, ~, hTab)
        gui_tabs.delete_tab(hTab);
    end

    function select_vars(~, ~)
        setappdata(0, 'varobs', varobs);
        window_title = 'Dynare GUI - Select observed variables';
        subtitle = 'Select which observed variables will be used:';
        field_name = 'varobs';
        base_field_name = 'all_varobs';
        column_names = {'Endogenous variable', 'LATEX name ', 'Long name ', 'Set as observed variable '};

        if ~isfield(model_settings, 'all_varobs') || isempty(model_settings.all_varobs)
            gui_tools.show_warning('Please define data file');
            return
        end

        h = gui_select_window(field_name, base_field_name, window_title, subtitle, column_names);
        uiwait(h);

        try
            handles.obs_table.Data = getappdata(0, 'varobs');
        catch ME
            gui_tools.show_error('Error selecting observed variables', ME, 'basic');
        end

    end

    function remove_selected(~, ~)
        if ~isempty(varobs)
            varobs([varobs{:,4}], :) = [];
            setappdata(0, 'varobs', varobs);
            handles.obs_table.Data = varobs;
        end
    end

    function create_panel_elements(panel_id)
        top = 1;
        dwidth = gui_size.default_width;
        dheight = gui_size.default_height;
        spc = gui_size.c_width;
        fheight = dheight/2;

        try
            num = 1;
            uicontrol(panel_id,'Style','text',...
                'String','Data file:',...
                'FontWeight', 'bold', ...
                'HorizontalAlignment', 'left','BackgroundColor', special_color,...
                'Units','normalized','Position',[spc*2 top-num*dheight dwidth fheight]);

            handles.data_file = uicontrol(panel_id,'Style','edit',...
                'String', project_info.data_file, ...
                'HorizontalAlignment', 'left',...
                'Units','normalized','Position',[spc*3+dwidth top-num*dheight dwidth fheight]);

            uicontrol(panel_id,'Style','text',...
                'String','*',...
                'HorizontalAlignment', 'left','BackgroundColor', special_color,...
                'Units','normalized','Position',[spc*3.3+dwidth*2 top-num*dheight spc fheight]);


            handles.data_file_button = uicontrol(panel_id,'Style','pushbutton',...
                'String', 'Select...',...
                'Units','normalized','Position',[spc*4.3+dwidth*2 top-num*dheight dwidth/2 fheight],...
                'Callback',{@select_file});

            num=num+1;
            uicontrol(panel_id,'Style','text',...
                'String','Sample starting date:',...
                'FontWeight', 'bold', ...
                'HorizontalAlignment', 'left','BackgroundColor', special_color,...
                'Units','normalized','Position',[spc*2 top-num*dheight dwidth fheight]);

            handles.first_obs =  uicontrol(panel_id,'Style','edit',...
                'String', project_info.first_obs, ...
                'TooltipString','For example: 1990Y, 1990Q3, 1990M11,1990W49',...
                'HorizontalAlignment', 'left',...
                'Units','normalized','Position',[spc*3+dwidth top-num*dheight dwidth fheight],...
                'Enable', 'off');

            uicontrol(panel_id,'Style','text',...
                'String','* (in Dynare dates format)',...
                'HorizontalAlignment', 'left','BackgroundColor', special_color,...
                'Units','normalized','Position',[spc*3.3+dwidth*2 top-num*dheight dwidth fheight]);


            num=num+1;
            uicontrol(panel_id,'Style','text',...
                'String','Data frequency:',...
                'FontWeight', 'bold', ...
                'HorizontalAlignment', 'left','BackgroundColor', special_color,...
                'Units','normalized','Position',[spc*2 top-num*dheight dwidth fheight]);


            handles.frequency = uicontrol(panel_id, 'Style', 'popup',...
                'String', {'annual','quarterly','monthly','weekly'},...
                'Value', project_info.freq,...
                'Units','normalized','Position',[spc*3+dwidth top-num*dheight dwidth fheight],...
                'Enable', 'off');


            uicontrol(panel_id,'Style','text',...
                'String','*',...
                'HorizontalAlignment', 'left','BackgroundColor', special_color,...
                'Units','normalized','Position',[spc*3.3+dwidth*2 top-num*dheight spc fheight]);

            num=num+1;
            uicontrol(panel_id,'Style','text',...
                'String','Number of observations:',...
                'FontWeight', 'bold', ...
                'HorizontalAlignment', 'left','BackgroundColor', special_color,...
                'Units','normalized','Position',[spc*2 top-num*dheight dwidth fheight]);

            handles.num_obs = uicontrol(panel_id,'Style','edit',...
                'String', project_info.nobs,...
                'HorizontalAlignment', 'left',...
                'Units','normalized','Position',[spc*3+dwidth top-num*dheight dwidth fheight],...
                'Enable', 'off');

            uicontrol(panel_id,'Style','text',...
                'String','*',...
                'HorizontalAlignment', 'left','BackgroundColor', special_color,...
                'Units','normalized','Position',[spc*3.3+dwidth*2 top-num*dheight spc fheight]);


            num=num+1.5;

            uicontrol(panel_id,'Style','text',...
                'String','Observed variables:',...
                'FontWeight', 'bold', ...
                'HorizontalAlignment', 'left','BackgroundColor', special_color,...
                'Units','normalized','Position',[spc*2 top-num*dheight dwidth fheight]);

            obs_variables(panel_id,varobs);

            uicontrol(panel_id,'Style','text',...
                'String','* = required field',...
                'HorizontalAlignment', 'left','BackgroundColor', special_color,...
                'FontAngle', 'italic', ...
                'Units','normalized','Position',[spc*0.5 spc dwidth dheight/2]);%[1 0 width v_size] );

        catch ME
            gui_tools.show_error('Error while displaying observed variables', ME, 'basic');
        end
    end

    function select_file(~, ~)
        try
            [fileName, pathName] = uigetfile({'*.m';'*.mat';'*.xls';'*.xlsx';'*.csv'}, 'Select data file');
            if fileName == 0
                return
            end
            if copyfile([pathName fileName], [project_info.project_folder filesep fileName])
                uiwait(msgbox('Data file copied to project folder', 'DynareGUI','modal'));
            end
            [~, basename, extension] = fileparts(fileName);
            first_obs = '';
            observable_vars = {};
            num_observables = [];
            freq = [];
            handles.first_obs.Enable = 'off';
            handles.frequency.Enable = 'off';
            try
                switch extension
                    case '.m'
                        [observable_vars, num_observables] = get_vars_from_m_file(basename);
                    case '.mat'
                        data = load(fileName);
                        observable_vars = fields(data);
                        num_observables = size(data.(observable_vars{1}), 1);
                    case { '.xls', '.xlsx' }
                        xls_sheet = 1;
                        xls_range = '';
                        [freq, init, data, observable_vars] = load_xls_file_data(fileName,xls_sheet,xls_range);
                        num_observables = size(data,1);
                        first_obs = gui_tools.dates2str(init);
                    case '.csv'
                        %TODO Check why load_csv_file_data is not working
                        %                         [freq,init,data,observable_vars] = load_csv_file_data(fileName);
                        %                         num_observables = size(data,1);
                        %                         first_obs =  gui_tools.dates2str(init);

                        [num, txt , ~] = xlsread(fileName);
                        first_cell = 1;
                        if isempty(txt{1,1}) % there is time of observation info in first column
                            first_cell = 2;
                            first_obs = txt{2,1};
                        end
                        observable_vars = txt(1,first_cell:end);%names of observable variables is in forst raw
                        num_observables = size(num,1);
                        handles.first_obs.Enable = 'on';
                end

                if ~isempty(freq)
                    switch freq
                        case 1
                            freq = 1;
                        case 4
                            freq = 2;
                        case 12
                            freq = 3;
                        case 52
                            freq = 4;
                    end
                    handles.frequency.Enable = 'off';
                else
                    handles.frequency.Enable = 'on';
                    freq = 2;
                end

                if isempty(first_obs)
                    handles.first_obs.Enable =  'on';
                    switch freq
                        case 1
                            first_obs = '1y';
                        case 2
                            first_obs = '1q1';
                        case 3
                            first_obs = '1m1';
                        case 4
                            first_obs = '1w1';
                    end
                else
                    handles.first_obs.Enable =  'off';
                end

                set_all_observables(observable_vars);

                project_info.data_file = fileName;
                handles.data_file.String = fileName;
                handles.first_obs.String = first_obs;
                handles.frequency.Value = freq;
                handles.num_obs.String = num_observables;
            catch ME
                gui_tools.show_error('Data file is not valid. Please specify a new data file.', ME, 'basic');
            end

        catch ME
            gui_tools.show_error('Error selecting data file', ME, 'basic');
        end
    end

    function set_all_observables(observable_vars)
        all_endo = model_settings.variables;
        if ~isempty(observable_vars)
            indx = ismember(all_endo(:,1),observable_vars);
            all_varobs = all_endo(indx, :);
        else
            all_varobs = all_endo;
        end
        model_settings.all_varobs = all_varobs;
    end

    function obs_variables(panel_id,data)
        handles.obs_table = uitable(panel_id,'Data',data,...
            'Units','normalized','Position',[0.02 0.1 0.96 0.45],...
            'ColumnName', {'Observed var.','LATEX name ', 'Long name ', 'Remove obs. var'},...
            'ColumnFormat', {'char','char','char', 'logical'},...
            'ColumnEditable', [false false false true ],...
            'ColumnWidth', {150, 150, 200, 120 }, ...
            'RowName',[],...
            'CellEditCallback',@savedata);
        function savedata(~, callbackdata)
            varobs{callbackdata.Indices(1), callbackdata.Indices(2)} = callbackdata.EditData;
        end
    end

    function cellArray = create_varobs_cell_array(data, data_tex, data_long, data_id)
        n = length(data);
        cellArray = cell(n,4);
        for i = 1:n
            cellArray{i,1} = data{i};
            cellArray{i,2} = data_tex{data_id(i)};
            cellArray{i,3} = data_long{data_id(i)};
            cellArray{i,4} = false;
        end
    end
end

function [observable_vars, num_observables] = get_vars_from_m_file(basename)
eval(basename)
clear basename
observable_vars = who;
num_observables = size(eval(observable_vars{1}), 1);
for i = 2:length(observable_vars)
    if size(eval(observable_vars{1}),1) ~= num_observables
        error('Data arrays are not the same size');
    end
end
end
