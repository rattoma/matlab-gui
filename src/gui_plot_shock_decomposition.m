function gui_plot_shock_decomposition(tabId)
% function gui_plot_shock_decomposition(tabId)
% interface for the DYNARE plot_shock_decomposition command
%
% INPUTS
%   tabId:  GUI tab element which displays plot_shock_decomposition command interface
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2019 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global project_info model_settings
global M_ oo_ options_ bayestopt_ estim_params_

bg_color = char(getappdata(0,'bg_color'));
special_color = char(getappdata(0,'special_color'));

handles = [];
gui_size = gui_tools.get_gui_elements_size(tabId);

% Panels
handles.uipanelVars = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelVars', ...
    'BackgroundColor', special_color, ...
    'Units', 'normalized', ...
    'Position', [0.01 0.09 0.48 0.82], ...
    'Title', '', ...
    'BorderType', 'none');

handles = gui_tabs.create_endo_vars(handles, 'plot_shock_decomposition');

handles.uipanelResults = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelVars', ...
    'BackgroundColor', bg_color,...
    'Units', 'normalized', ...
    'Position', [0.51 0.09 0.48 0.82], ...
    'Title', 'Command and results options:');

uipanelResults_CreateFcn();

% Text
uicontrol( ...
    'Parent', tabId, ...
    'Style', 'text', ...
    'BackgroundColor', bg_color,...
    'Units', 'normalized', ...
    'Position', [0.01 0.92 0.48 0.05], ...
    'FontWeight', 'bold', ...
    'String', 'Select endogenous variables for plot:', ...
    'HorizontalAlignment', 'left');

% Buttons
handles.pushbuttonPlotShockDecomposition = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonSimulation', ...
    'Style', 'pushbutton', ...
    'Units', 'normalized', ....
    'Position', [gui_size.space gui_size.bottom gui_size.button_width gui_size.button_height], ...
    'String', 'Run', ...
    'Callback', @pushbuttonPlotShockDecomposition_Callback);

handles.pushbuttonReset = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonReset', ...
    'Style', 'pushbutton', ...
    'Units', 'normalized', ...
    'Position', [gui_size.space*2+gui_size.button_width gui_size.bottom gui_size.button_width gui_size.button_height], ...
    'String', 'Reset', ...
    'Callback', @pushbuttonReset_Callback);

    function pushbuttonReset_Callback(~, ~)
        handles.endoTable.Data(:, 1) = {false};
        handles.colormap.Value = 1;
        handles.type.Value = 1;
        handles.realtime.Value = 1;
        handles.graph_format.Value = 1;
        handles.nodisplay.Value = 0;
        handles.detail_plot.Value = 0;
        handles.interactive.Value = 0;
        handles.screen_shocks.Value = 0;
        handles.steadystate.Value = 0;
        handles.write_xls.Value = 0;
        handles.use_shock_groups.Value = 0;
        handles.fig_name.String = '';
        handles.vintage.String = '';
        handles.plot_init_date.String = '';
        handles.plot_end_date.String = '';
    end

handles.pushbuttonCloseAll = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonSimulation', ...
    'Style', 'pushbutton', ...
    'Units', 'normalized', ...
    'Position', [gui_size.space*3+gui_size.button_width*2 gui_size.bottom gui_size.button_width gui_size.button_height], ...
    'String', 'Close all output figures', ...
    'Enable', 'on', ...
    'Callback', @pushbuttonCloseAll_Callback);

    function pushbuttonCloseAll_Callback(~, ~)
        gui_tools.close_all_figures();
    end

handles.pushbuttonClose = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonReset', ...
    'Units', 'normalized', ...
    'Position', [gui_size.space*4+gui_size.button_width*3 gui_size.bottom gui_size.button_width gui_size.button_height], ...
    'String', 'Close this tab', ...
    'Callback',{@close_tab, tabId});

    function close_tab(~, ~, hTab)
        gui_tabs.delete_tab(hTab);
    end

    function uipanelResults_CreateFcn()
        top = 1;
        dwidth = gui_size.default_width;
        dheight = gui_size.default_height;
        spc = gui_size.c_width;

        num = 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'text8', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'colormap:', ...
            'HorizontalAlignment', 'left');

        handles.colormap = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'colormap', ...
            'Style', 'popupmenu', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'String', {'','parula', 'jet', 'hsv', 'hot', 'cool','spring','summer','autumn','winter','gray','bone','copper','pink','lines','colorcube','prism','flag','white'}, ...
            'Value', 1);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'text8', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'nodisplay:', ...
            'HorizontalAlignment', 'left');

        handles.nodisplay = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'nodisplay', ...
            'Style', 'check', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'Value', 0);

                num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'text8', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'graph_format:', ...
            'HorizontalAlignment', 'left');

        handles.graph_format = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'graph_format', ...
            'Style', 'popupmenu', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'String', {'none','eps','pdf','fig'}, ...
            'Value', 1);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'text8', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'detail_plot:', ...
            'HorizontalAlignment', 'left');

        handles.detail_plot = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'detail_plot', ...
            'Style', 'check', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'Value', 0);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'text8', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'interactive:', ...
            'HorizontalAlignment', 'left');

        handles.interactive = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'interactive', ...
            'Style', 'check', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'Value', 0);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'text8', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'screen_shocks:', ...
            'HorizontalAlignment', 'left');

        handles.screen_shocks = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'screen_shocks', ...
            'Style', 'check', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'Value', 0);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'text8', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'steadystate:', ...
            'HorizontalAlignment', 'left');

        handles.steadystate = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'steadystate', ...
            'Style', 'check', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'Value', 0);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'text8', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'write_xls:', ...
            'HorizontalAlignment', 'left');

        handles.write_xls = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'write_xls', ...
            'Style', 'check', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'Value', 0);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'text8', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'fig_name:', ...
            'HorizontalAlignment', 'left');

        handles.fig_name = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'fig_name', ...
            'Style', 'edit', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'Value', 0);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'text8', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'type:', ...
            'HorizontalAlignment', 'left');

        handles.type = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'type', ...
            'Style', 'popupmenu', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'String', {'qoq','yoy','aoa'}, ...
            'Value', 1);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'text8', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'realtime:', ...
            'HorizontalAlignment', 'left');

        realtimestring = {'standard historical shock decomposition', ...
            'realtime historical shock decomposition', ...
            'conditional realtime shock decomposition', ...
            'realtime forecast shock decomposition'};

        handles.realtime = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'realtime', ...
            'Style', 'popupmenu', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'String', realtimestring, ...
            'Value', 1);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'vintage', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'vintage:', ...
            'HorizontalAlignment', 'left');

        handles.vintage = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'vintage', ...
            'String', '0', ...
            'Style', 'edit', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'Value', 0);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'plot_init_date', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'plot_init_date:', ...
            'HorizontalAlignment', 'left');

        handles.plot_init_date = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'plot_init_date', ...
            'String', '', ...
            'Style', 'edit', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'Value', 0);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'plot_end_date', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'plot_end_date:', ...
            'HorizontalAlignment', 'left');

        handles.plot_end_date = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'plot_end_date', ...
            'String', '', ...
            'Style', 'edit', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'Value', 0);

        num = num + 1;
        uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'use_shock_groups', ...
            'Style', 'text', ...
            'Units', 'normalized', ...
            'Position', [spc*3 top-num*dheight dwidth*1.5 dheight/2], ...
            'String', 'use_shock_groups:', ...
            'HorizontalAlignment', 'left');

        shock_group_options = {''};
        if isfield(M_, 'shock_groups')
            shock_group_options = [shock_group_options; fieldnames(M_.shock_groups)];
        end
        handles.use_shock_groups = uicontrol( ...
            'Parent', handles.uipanelResults, ...
            'Tag', 'use_shock_groups', ...
            'Style', 'popupmenu', ...
            'Units', 'normalized', ...
            'Position', [spc*3+dwidth*1.5 top-num*dheight dwidth*2 dheight/2], ...
            'String', shock_group_options);
    end

    function pushbuttonPlotShockDecomposition_Callback(hObject, ~)
        old_options = options_;

        if isempty(str2double(handles.vintage.String)) ...
                || ~isint(str2double(handles.vintage.String)) ...
                || str2double(handles.vintage.String) < 0
            gui_tools.show_warning('Vintage must be an integer >= 0');
            uicontrol(hObject);
            return
        end
        options_.plot_shock_decomp.vintage = str2double(handles.vintage.String);
        
        if ~isempty(handles.plot_init_date.String)
            if isint(str2double(handles.plot_init_date.String))
                options_.plot_shock_decomp.plot_init_date = str2double(handles.plot_init_date.String);
            elseif isdate(handles.plot_init_date.String)
                options_.plot_shock_decomp.plot_init_date = dates(handles.plot_init_date.String);
            else
                gui_tools.show_warning('Initial date must be an integer or a date');
                uicontrol(hObject);
                return
            end
        else
            options_.plot_shock_decomp.plot_init_date = '';
        end

        if ~isempty(handles.plot_end_date.String)
            if isint(str2double(handles.plot_end_date.String))
                options_.plot_shock_decomp.plot_end_date = str2double(handles.plot_end_date.String);
            elseif isdate(handles.plot_end_date.String)
                options_.plot_shock_decomp.plot_end_date = dates(handles.plot_end_date.String);
            else
                gui_tools.show_warning('End date must be an integer or a date');
                uicontrol(hObject);
                return
            end
        else
            options_.plot_shock_decomp.plot_end_date = '';
        end

        if handles.colormap.Value ~= 1
            options_.plot_shock_decomp.colormap = handles.colormap.String{handles.colormap.Value};
        end

        options_.plot_shock_decomp.nodisplay = false;
        if handles.nodisplay.Value
            options_.plot_shock_decomp.nodisplay = true;
        end

        options_.graph_format = {};
        if handles.graph_format.Value ~= 1
            options_.graph_format = handles.graph_format.String(handles.graph_format.Value);
        end

        options_.type = handles.type.String;

        options_.plot_shock_decomp.detail_plot = false;
        if handles.detail_plot.Value
            options_.plot_shock_decomp.detail_plot = true;
        end

        options_.plot_shock_decomp.interactive = false;
        if handles.interactive.Value
            options_.plot_shock_decomp.interactive = true;
        end

        options_.plot_shock_decomp.screen_shocks = false;
        if handles.screen_shocks.Value
            options_.plot_shock_decomp.screen_shocks = true;
        end

        if ~isempty(handles.fig_name.String)
            options_.plot_shock_decomp.fig_name = handles.fig_name.String;
        end

        options_.plot_shock_decomp.steadystate = false;
        if handles.steadystate.Value
            options_.plot_shock_decomp.steadystate = true;
        end

        options_.plot_shock_decomp.write_xls = false;
        if handles.write_xls.Value
            options_.plot_shock_decomp.write_xls = true;
        end
        
        options_.plot_shock_decomp.use_shock_groups = handles.use_shock_groups.String{handles.use_shock_groups.Value};
        options_.plot_shock_decomp.realtime = handles.realtime.Value - 1;

        gui_tools.project_log_entry('Running plot shock decomposition', '...');
        [jObj, guiObj] = gui_tools.create_animated_screen('Running plot shock decomposition, please wait...', tabId);
        var_list_ = handles.endoTable.Data([handles.endoTable.Data{:,1}], 2);

        % computations take place here
        try
            plot_shock_decomposition(M_, oo_, options_, var_list_);
            jObj.stop;
            jObj.setBusyText('All done!');
            project_info.modified = true;
            project_info.plot_shock_decomposition_run = true;
        catch ME
            jObj.stop;
            jObj.setBusyText('Done with errors!');
            options_ = old_options;
            gui_tools.show_error('Error in execution of shock decomposition command', ME, 'extended');
            uicontrol(hObject);
        end
        delete(guiObj);
        options_ = old_options;
    end
end
