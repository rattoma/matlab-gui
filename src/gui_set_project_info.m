function gui_set_project_info()
% function gui_set_project_info()
% set default project_info structure
%
% INPUTS
%   none
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2019 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global project_info

%% Define
project_info = struct;

project_info.project_name = '';
project_info.project_folder = '';
project_info.project_model_stochastic = 1;
project_info.project_description = '';
project_info.default_forecast_periods = 20;
project_info.modified = true;
project_info.mod_file_already_run = false;

project_info.first_obs = 1;
project_info.freq = 2;
project_info.first_obs_date = dates('1q1');
project_info.last_obs_date = dates('1q1');
project_info.nobs = '';
project_info.data_file = '';

project_info.new_data_format = 0;

project_info.shock_decomposition_run = false;
project_info.realtime_shock_decomposition_run = false;
project_info.conditional_forecast_run = false;
project_info.stochastic_simulation_run = false;
project_info.deterministic_simulation_run = false;
project_info.estimation_run = false;
project_info.observed_variables_set = false;

gui_auxiliary.create_dynare_gui_structure;

end
