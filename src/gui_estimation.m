function gui_estimation(tabId)
% function gui_estimation(tabId)
% interface for the DYNARE estimation command
%
% INPUTS
%   tabId:      GUI tab element which displays estimation command interface
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2003-2019 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global project_info dynare_gui_ options_ oo_ model_settings M_

bg_color = char(getappdata(0,'bg_color'));
special_color = char(getappdata(0,'special_color'));

handles = [];
gui_size = gui_tools.get_gui_elements_size(tabId);

do_not_check_all_results = 0;

% Panels
handles.uipanelResults = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelVars', 'BackgroundColor', special_color,...
    'Units', 'normalized', 'Position', [0.01 0.18 0.48 0.73], ...
    'Title', '', 'BorderType', 'none');

uipanelResults_CreateFcn;

handles.uipanelVars = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelVars', 'BackgroundColor', special_color,...
    'Units', 'normalized', 'Position', [0.51 0.18 0.48 0.73], ...
    'Title', '', 'BorderType', 'none');

handles = gui_tabs.create_endo_vars(handles, 'estimation');

handles.uipanelComm = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelCommOptions', ...
    'UserData', zeros(1,0), 'BackgroundColor', bg_color, ...
    'Units', 'normalized', 'Position', [0.01 0.09 0.98 0.09], ...
    'Title', 'Current command options:');

% Text
uicontrol( ...
    'Parent', tabId, 'Tag', 'text7', ...
    'Style', 'text', 'BackgroundColor', bg_color,...
    'Units','normalized','Position',[0.01 0.92 0.48 0.05],...
    'FontWeight', 'bold', ...
    'String', 'Select wanted results:', ...
    'HorizontalAlignment', 'left');

uicontrol( ...
    'Parent', tabId, 'Tag', 'text7', ...
    'Style', 'text', 'BackgroundColor', bg_color,...
    'Units','normalized','Position',[0.51 0.92 0.48 0.05],...
    'FontWeight', 'bold', ...
    'String', 'Select endogenous variables that will be used in estimation:', ...
    'HorizontalAlignment', 'left');

if ~isfield(model_settings, 'estimation')
    model_settings.estimation = struct();
end
comm_str = gui_tools.command_string('estimation', model_settings.estimation);
check_all_result_option();
if isfield(model_settings.estimation, 'consider_all_endogenous')
    handles.endoTable.Data(:, 1) = {logical(model_settings.estimation.consider_all_endogenous)};
end
if isfield(model_settings.estimation,'consider_only_observed') && model_settings.estimation.consider_only_observed
    select_only_observed();
end

handles.estimation = uicontrol( ...
    'Parent', handles.uipanelComm, ...
    'Tag', 'estimation', ...
    'Style', 'text', 'BackgroundColor', bg_color,...
    'Units', 'normalized', 'Position', [0.01 0.01 0.98 0.98], ...
    'FontAngle', 'italic', ...
    'String', comm_str, ...
    'TooltipString', comm_str, ...
    'HorizontalAlignment', 'left');

% Buttons
handles.pushbuttonEstimation = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonSimulation', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space gui_size.bottom gui_size.button_width_small gui_size.button_height],...
    'String', 'Run estimation', ...
    'Interruptible','on',...
    'Callback', @pushbuttonEstimation_Callback);

handles.pushbuttonReset = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonReset', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space*2+gui_size.button_width_small gui_size.bottom gui_size.button_width_small gui_size.button_height],...
    'String', 'Reset', ...
    'Callback', @pushbuttonReset_Callback);

handles.pushbuttonClose = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonReset', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space*3+gui_size.button_width_small*2 gui_size.bottom gui_size.button_width_small gui_size.button_height],...
    'String', 'Close this tab', ...
    'Callback',{@close_tab,tabId});

handles.pushbuttonResults = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonSimulation', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space*4+gui_size.button_width_small*3 gui_size.bottom gui_size.button_width_small gui_size.button_height],...
    'String', 'Browse results...', ...
    'Enable', 'on',...
    'Callback', @pushbuttonResults_Callback);

handles.pushbuttonCloseAll = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonSimulation', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space*5+gui_size.button_width_small*4 gui_size.bottom gui_size.button_width_small gui_size.button_height],...
    'String', 'Close all output figures', ...
    'Enable', 'On',...
    'Callback', @pushbuttonCloseAll_Callback);

handles.pushbuttonCommandDefinition = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonCommandDefinition', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[1-gui_size.space-gui_size.button_width_small gui_size.bottom gui_size.button_width_small gui_size.button_height],...
    'String', 'Define command options ...', ...
    'Callback', @pushbuttonCommandDefinition_Callback);
        
    function uipanelResults_CreateFcn()
        results = dynare_gui_.est_results;
        names = fieldnames(results);
        num_groups = size(names,1);
        current_result = 1;
        maxDisplayed = 12;

        % Create tabs
        handles.result_tab_group = uitabgroup(handles.uipanelResults,'Position',[0 0 1 1]);

        for iii = 1:num_groups
            create_tab(iii, names{iii});
        end

        function create_tab(num,group_name)
            new_tab = uitab(handles.result_tab_group, 'Title',group_name , 'UserData', num);
            tabs_panel = uipanel('Parent', new_tab,'BackgroundColor', 'white', 'BorderType', 'none');

            tabs_panel.Units = 'characters';
            pos = tabs_panel.Position;
            tabs_panel.Units = 'Normalized';

            maxDisplayed = floor(pos(4)/2) - 3 ;
            top_position = pos(4) - 6;

            group = results.(group_name);
            numTabResults = size(group,1);

        % Create slider
        if numTabResults > maxDisplayed
            uicontrol('Style', 'slider',...
                'Parent', tabs_panel, ...
                'Min', 0, ...
                'Max', numTabResults - maxDisplayed, ...
                'Value', numTabResults - maxDisplayed , ...
                'Units', 'normalized', ...
                'Position', [0.968 0 .03 1], ...
                'Callback', {@scrollPanel_Callback,num,numTabResults});
        end

        for ii = 1:numTabResults
            visible = 'on';
            if ii > maxDisplayed
                visible = 'off';
            end

            tt_string = combine_desriptions(group{ii,3});
            handles.tab_results(num,ii) = uicontrol('Parent', tabs_panel , 'style','checkbox',...  %new_tab
                'Units','characters',...  %'Units','normalized',...
                'position',[3 top_position-(2*ii) 60 2],...%'Position',[0.03 0.98-ii*0.08 0.9 .08],...
                'TooltipString', gui_tools.tool_tip_text(tt_string,50),...
                'string',group{ii,1},...
                'BackgroundColor', special_color,...
                'Visible', visible,...
                'Callback',{@checkbox_Callback,group{ii,3}} );
            handles.results(current_result) = handles.tab_results(num, ii);
            current_result = current_result + 1; %all results on all tabs
        end

        handles.num_results=current_result-1;

            function checkbox_Callback(hObject, ~, comm_option)
                if isempty(comm_option)
                    return
                end
                num_options = size(comm_option, 2);
                if isfield(model_settings, 'estimation')
                    user_options = model_settings.estimation;
                else
                    user_options = struct();
                end
                if hObject.Value
                    for i = 1:num_options
                        option_value = comm_option(i).option;
                        indx = findstr(comm_option(i).option, '.');
                        if ~isempty(indx)
                            option_value = option_value(1:indx(1)-1);
                        end
                        if comm_option(i).flag
                            if comm_option(i).used
                                user_options.(option_value) = 1;
                            else
                                if isfield(user_options, comm_option(i).option)
                                    user_options = rmfield(user_options, option_value);
                                end
                            end
                        else
                            user_options.(option_value) = comm_option(i).value_if_selected;
                        end
                    end
                else
                    for i = 1:num_options
                        option_value = comm_option(i).option;
                        indx = findstr(comm_option(i).option, '.');
                        if ~isempty(indx)
                            option_value = option_value(1:indx(1)-1);
                        end
                        if comm_option(i).flag
                            if comm_option(i).used
                                user_options = rmfield(user_options, option_value);
                            end
                        else
                            if ~isempty(comm_option(i).value_if_not_selected)
                                user_options.(option_value) = comm_option(i).value_if_not_selected;
                            else
                                user_options = rmfield(user_options, option_value);
                            end
                        end
                    end
                end

                model_settings.estimation =  user_options;
                comm_str = gui_tools.command_string('estimation', user_options);
                handles.estimation.String = comm_str;
                handles.estimation.TooltipString = comm_str;

                if ~do_not_check_all_results
                    check_all_result_option();
                end
            end

            function str = combine_desriptions(comm_option)
                str = '';
                for i = 1:size(comm_option,2)
                    str = [str, comm_option(i).description];
                end
            end

            function scrollPanel_Callback(hObject, ~, tab_index, num_results)
                move = num_results - maxDisplayed - floor(hObject.Value);
                for iiii = 1:num_results
                    if iiii <= move || iiii > move+maxDisplayed
                        handles.tab_results(tab_index, iiii).Visible = 'off';
                    else
                        handles.tab_results(tab_index, iiii).Visible = 'on';
                        handles.tab_results(tab_index, iiii).Position = [3 top_position-(iiii-move)*2 60 2];
                    end
                end
            end
        end
    end

    function check_all_result_option()
        results = dynare_gui_.est_results;
        user_options = model_settings.estimation;
        names = fieldnames(results);
        do_not_check_all_results = 1;

        for num=1:size(names,1)
            group = results.(names{num});
            numTabResults = size(group,1);
            ii = 1;
            while ii <= numTabResults
                comm_option = group{ii,3};
                if isempty(comm_option)
                    selected = 0;
                else
                    selected = 1;
                end
                jj = 1;
                while jj <= size(comm_option,2) && selected %only if result requires specific options
                    if comm_option(jj).flag
                        if comm_option(jj).used
                            if ~isfield(user_options, comm_option(jj).option)
                                selected = 0;
                            end
                        else
                            if isfield(user_options, comm_option(jj).option)
                                selected = 0;
                            end
                        end
                    else
                        if ~isfield(user_options, comm_option(jj).option)
                            selected = 0;
                        else
                            if user_options.(comm_option(jj).option) ~= comm_option(jj).value_if_selected
                                selected = 0;
                            end
                        end
                    end
                    jj = jj+1;
                end
                handles.tab_results(num,ii).Value = selected;
                ii = ii+1;
            end
        end
        do_not_check_all_results = 0;
    end

    function pushbuttonEstimation_Callback(hObject, ~)
        handles.pushbuttonResults.Enable = 'off';
        old_options = options_;
        old_oo = oo_;
        try
            backup = gui_auxiliary.backup_options(options_);
            options_ = default_option_values(M_);
            options_ = gui_auxiliary.add_backup_to_options(options_, backup);
            if ~isempty(model_settings.estimation)
                names = fieldnames(model_settings.estimation);
                for ii = 1: size(names,1)
                    if isempty(model_settings.estimation.(names{ii}))
                        gui_auxiliary.set_command_option(names{ii}, 1, 'check_option');
                    else
                        gui_auxiliary.set_command_option(names{ii}, model_settings.estimation.(names{ii}));
                    end
                end
            end
            options_.order = 1;
            gui_tools.project_log_entry('Running estimation','...');
            [jObj, guiObj] = gui_tools.create_animated_screen('Running estimation, please wait...', tabId);
            model_settings.varlist_.estimation = handles.endoTable.Data([handles.endoTable.Data{:,1}], 2);
            dynare_estimation(model_settings.varlist_.estimation);

            jObj.stop;
            jObj.setBusyText('All done!');

            handles.pushbuttonResults.Enable = 'on';
            project_info.modified = true;
            project_info.estimation_run = true;
            gui_set_menus(true);
        catch ME
            jObj.stop;
            jObj.setBusyText('Done with errors!');
            gui_tools.show_error('Error in execution of estimation command', ME, 'extended');
            uicontrol(hObject);
            options_ = old_options;
            oo_ = old_oo;
        end
        delete(guiObj);
    end

    function pushbuttonReset_Callback(~, ~)
        handles.endoTable.Data(:, 1) = {false};
        for ii = 1:handles.num_results
            handles.results(ii).Value = 0;
        end
        model_settings.estimation = struct();
        comm_str = gui_tools.command_string('estimation', model_settings.estimation);
        handles.estimation.String = comm_str;
        handles.estimation.TooltipString = comm_str;
    end

    function pushbuttonCommandDefinition_Callback(~, ~)
        old_comm = model_settings.estimation;
        h = gui_define_comm_options(dynare_gui_.estimation,'estimation');
        uiwait(h);
        try
            new_comm = getappdata(0, 'estimation');
            
            if isfield(new_comm,'consider_all_endogenous')
                handles.endoTable.Data(:, 1) = {logical(new_comm.consider_all_endogenous)};
            end
            
            if isfield(new_comm, 'consider_only_observed') && new_comm.consider_only_observed
                select_only_observed();
            end
            
            if ~isempty(new_comm)
                model_settings.estimation = new_comm;
            end
            
            comm_str = gui_tools.command_string('estimation', new_comm);
            handles.estimation.String = comm_str;
            handles.estimation.TooltipString = comm_str;
            check_all_result_option();
            gui_tools.project_log_entry('Defined command estimation', comm_str);
        catch ME
            gui_tools.show_error('Error in defining estimation command', ME, 'basic');
            model_settings.estimation = old_comm;
        end
    end

    function select_only_observed()
        handles.endoTable.Data(:,1) = {false};
        for ii = 1:length(options_.varobs)
            handles.endoTable.Data(strcmp(options_.varobs{ii}, handles.endoTable.Data(:,2)), 1) = {true};
        end
    end

    function close_tab(~, ~, hTab)
        gui_tabs.delete_tab(hTab);
    end

    function pushbuttonResults_Callback(~, ~)
        gui_results('estimation', dynare_gui_.est_results);
    end

    function pushbuttonCloseAll_Callback(~, ~)
        gui_tools.close_all_figures();
    end
end
