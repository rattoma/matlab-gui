function gui_open_project(hObject)
% function gui_open_project(hObject)
% opens Dynare_GUI project file
%
% INPUTS
%   hObject:    handle of main application window
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2003-2019 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global project_info

[fileName, pathName] = uigetfile('*.dproj', 'Select Dynare GUI project file:');
if fileName == 0
    return
end

try
    [~, project_name, ext] = fileparts(fileName);
    if strcmp(ext, '.dproj') && ~isempty(project_name)
        tabId = gui_tabs.add_tab(hObject, ['Project: ' project_name]);
        data = load([pathName fileName], '-mat');
        flds = fieldnames(data);
        for i = 1:length(flds)
            assignin('base', flds{i}, data.(flds{i}));
        end

        if ~strcmp([project_info.project_folder filesep], pathName)
            warnStr = sprintf('Project has moved to different folder/path since last modification. New project folder has been set accordingly.!\n\nIf needed, please copy .mod file and data file to the new location (new project folder).');
            gui_tools.show_warning(warnStr);
            setappdata(0,'new_project_location',true);
            project_info.project_folder = pathName(1:length(pathName)-1);
        else
            setappdata(0,'new_project_location',false);
        end

        evalin('base', ['diary(''' project_name '.log'');']);

        % important to create after project_info is set
        gui_auxiliary.create_dynare_gui_structure;

        gui_project(tabId, 'Open');

        % change current folder to project folder
        cd(project_info.project_folder);

        %enable menu options
        gui_set_menus(true);

        project_info.modified = false;
        gui_tools.project_log_entry('Project Open', ...
            ['project_name=' project_info.project_name '; project_folder=' project_info.project_folder]);
    end
catch ME
    gui_tools.show_error('Error opening project file', ME, 'basic');
end
end
