function gui_cond_forecast_preliminary(tabId, hObject, handles)
% function gui_cond_forecast(tabId)
% interface for the DYNARE imcforecast command
%
% INPUTS
%   tabId    GUI tab element which displays the precursor to conditional
%            forecast
%   hObject  handle to output_conditional_forecast (see GCBO)
%   handles  structure with handles and user data (see GUIDATA)
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2019 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.


global project_info model_settings oo_

bg_color = char(getappdata(0, 'bg_color'));
gui_size = gui_tools.get_gui_elements_size(tabId);

% Layout

handles = struct();
uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'text7', ...
    'Style', 'text','BackgroundColor', bg_color,...
    'Units','normalized','Position',[0.01 0.92 0.48 0.05],...
    'FontWeight', 'bold', ...
    'String', 'Choose before conditional forecast:', ...
    'HorizontalAlignment', 'left');

% Panel
panel_id = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelVars', 'BackgroundColor', bg_color,...
    'Units', 'normalized', 'Position', [0.01 0.18 0.48 0.73], ...
    'Title', '', ...
    'BorderType', 'none');

% Panel contents
top = 1;
dwidth = gui_size.default_width;
dheight = gui_size.default_height;
spc = gui_size.c_width;
num = 1;

%% Baseline type
uicontrol( ...
    panel_id, ...
    'Style', 'text', ...
    'String', 'Baseline Type:', ...
    'FontWeight', 'bold', ...
    'HorizontalAlignment', 'left', ...
    'BackgroundColor', bg_color, ...
    'Units', 'normalized', ...
    'Position', [spc*2 top-num*dheight dwidth*2 dheight/2]);
baseline_options = {'Choose baseline data ...'; 'File'};
if isfield(oo_, 'forecast')
    baseline_options{end+1} = 'Forecast';
end
if isfield(oo_, 'MeanForecast')
    baseline_options{end+1} = 'Mean Forecast';
end
handles.baseline_source = uicontrol( ...
    'Parent', panel_id, ...
    'Style', 'popup', ...
    'BackgroundColor', bg_color, ...
    'Units', 'normalized', ...
    'Position', [spc*4+dwidth*1 top-num*dheight dwidth*3 dheight/2], ...
    'String', baseline_options);


%% Baseline file
num = num + 2;
uicontrol( ...
    panel_id, ...
    'Style', 'text', ...
    'String', 'Baseline File:', ...
    'FontWeight', 'bold', ...
    'HorizontalAlignment', 'left', ...
    'BackgroundColor', bg_color, ...
    'Units', 'normalized', ...
    'Position', [spc*2 top-num*dheight dwidth*2 dheight/2]);

handles.baseline_file = uicontrol( ...
    panel_id, ...
    'Style', 'edit', ...
    'HorizontalAlignment', 'left',...
    'Units', 'normalized', ...
    'Position', [spc*3+dwidth*1.2 top-num*dheight dwidth*2 dheight/2]);

handles.baseline_file_button = uicontrol( ...
    panel_id, ...
    'Style', 'pushbutton', ...
    'String', 'Select...', ...
    'Units', 'normalized', ...
    'Position', [spc*4+dwidth*3.3 top-num*dheight dwidth dheight/2], ...
    'Callback', @select_file);

    function select_file(~, ~)
        [fileName, pathName] = uigetfile({'*.m';'*.mat';'*.xls';'*.xlsx';'*.csv'}, 'Select baseline file');
        if fileName == 0
            return
        end
        if ~strcmp([pathName fileName], fullfile(project_info.project_folder, fileName))
            if exist(fullfile(project_info.project_folder, fileName), 'file') == 2
                gui_tools.show_warning(['Trying to copy to ' fullfile(project_info.project_folder, fileName) ...
                    ' but file exsits. Please delete and try again']);
                return
            end
            copyfile([pathName fileName], fullfile(project_info.project_folder, fileName));
        end
        project_info.data_file = fileName;
        handles.baseline_file.String = fileName;
    end

%% Choose Exogenous Variables
num = num + 1;
uicontrol( ...
    'Parent', panel_id, ...
    'Tag', 'baseline', ...
    'Style', 'text', ...
    'BackgroundColor', bg_color, ...
    'Units', 'normalized', ...
    'Position', [spc*2 top-num*dheight dwidth*5 dheight/2], ...
    'String', 'Controlled Exogenous Variables:', ...
    'FontWeight', 'bold', ...
    'HorizontalAlignment', 'left');
exodata = cell(size(model_settings.shocks, 1), 2);
exodata(:, 1) = {false};
exodata(:, 2) = model_settings.shocks(:, 1);
for idx = 1:length(find(strcmp(model_settings.shocks(:,1), model_settings.shocks(:,3)) == 0))
    exodata{idx, 2} = [model_settings.shocks{idx,1},' (',  model_settings.shocks{idx,3}, ')'];
end
column_names = {'Active', 'Name'};
editable = [true false];
currentPanel = uipanel( ...
    'Parent', panel_id, ...
    'BackgroundColor', 'white', ...
    'BorderType', 'none', ...
    'Position', [spc*2 top-num*1.1*dheight*2 dwidth*5 dheight*4]);
handles.exo_table = uitable(currentPanel, ...
    'Data', exodata, ...
    'Units', 'normalized', ...
    'ColumnName', column_names, ...
    'ColumnEditable', editable, ...
    'RowName', [], ...
    'Position', [0 0 1 1]);

%% Choose endogenous
handles.uipanelVars = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelVars', ...
    'BackgroundColor', bg_color,...
    'Units', 'normalized', 'Position', [0.51 .15 0.48 0.73], ...
    'Title', '', ...
    'BorderType', 'none');
uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'Constrained Endogenous Variables', ...
    'Style', 'text', ...
    'BackgroundColor', bg_color, ...
    'Units', 'normalized', ...
    'Position', [.51 .9 0.48 0.05], ...
    'String', 'Constrained Endogenous:', ...
    'FontWeight', 'bold', ...
    'HorizontalAlignment', 'left');
handles = gui_tabs.create_endo_vars(handles, 'conditional_forecast');

%% Buttons
handles.save_and_go_to_cond_forecast = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonCondForecast', ...
    'Style', 'pushbutton', ...
    'Units','normalized', ...
    'Position', [gui_size.space gui_size.bottom gui_size.button_width*1.2 gui_size.button_height],...
    'String', 'Save & Go to Cond. Forecast', ...
    'Callback', {@saveAndGoToCondForecast, hObject});

    function saveAndGoToCondForecast(~, ~, hObject)
        if isempty(handles.baseline_file.String)
            if handles.baseline_source.Value == 1 || handles.baseline_source.Value == 2
                gui_tools.show_warning('You must either select a baseline file or a baseline file type that is not `File`')
                return
            end
        else
            if ~(handles.baseline_source.Value == 1 || handles.baseline_source.Value == 2)
                gui_tools.show_warning('You selected a baseline file but the baseline type does not correspond to this choice')
                return
            end
        end
        if ~any([handles.endoTable.Data{:, 1}])
            gui_tools.show_warning('You must select at least one endogenous variable')
            return
        end
        if ~any([handles.exo_table.Data{:, 1}])
            gui_tools.show_warning('You must select at least one exogenous variable')
            return
        end
        if sum([handles.endoTable.Data{:, 1}]) ~= sum([handles.exo_table.Data{:, 1}])
            gui_tools.show_warning('The number of constrained endogenous variables must be equal to the number of controlled exogenous variables')
            return
        end
        model_settings.conditional_forecast_options.controlled_varexo = handles.exo_table.Data([handles.exo_table.Data{:, 1}], 2);
        model_settings.conditional_forecast_options.endo = handles.endoTable.Data([handles.endoTable.Data{:, 1}], 2);
        model_settings.conditional_forecast_options.baseline_file = handles.baseline_file.String;
        model_settings.conditional_forecast_options.baseline_source = handles.baseline_source.String{handles.baseline_source.Value};
        gui_tabs.delete_tab(tabId);
        gui_cond_forecast(gui_tabs.add_tab(hObject, 'Cond. forecast'));
    end

handles.clear_choices = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonReset', ...
    'Style', 'pushbutton', ...
    'Units','normalized', ...
    'Position',[gui_size.space*4+gui_size.button_width gui_size.bottom gui_size.button_width gui_size.button_height],...
    'String', 'Reset', ...
    'Callback', @clearChoices);

    function clearChoices(~, ~)
        handles.baseline_source.Value = 1;
        handles.baseline_file.String = '';
        handles.baseline_file.Enable = 'Off';
        handles.endoTable.Data(:, 1) = {false};
        handles.exo_table.Data(:, 1) = {false};
    end

handles.close_tab = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonReset', ...
    'Style', 'pushbutton', ...
    'Units','normalized', ...
    'Position', [gui_size.space*5+gui_size.button_width*2 gui_size.bottom gui_size.button_width gui_size.button_height],...
    'String', 'Close this tab', ...
    'Callback', @closeTab);

    function closeTab(~, ~)
        gui_tabs.delete_tab(tabId);
    end
end
