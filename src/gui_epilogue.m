function gui_epilogue(tabId)
% function gui_epilogue(tabId)
% interface for the DYNARE epilogue command
%
% INPUTS
%   tabId:  GUI tab element which displays epilogue command interface
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2019-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global M_

bg_color = char(getappdata(0, 'bg_color'));
special_color = char(getappdata(0, 'special_color'));

handles = [];
gui_size = gui_tools.get_gui_elements_size(tabId);

% --- PANELS -------------------------------------
handles.equationPanel = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelVars', ...
    'BackgroundColor', special_color,...
    'Units', 'normalized', ...
    'Position', [0.01 0.09 0.48 0.82], ...
    'Title', '', ...
    'BorderType', 'none');

uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'text7', ...
    'Style', 'text', ...
    'BackgroundColor', bg_color,...
    'Units', 'normalized', ...
    'Position', [0.01 0.92 0.48 0.05], ...
    'FontWeight', 'bold', ...
    'String', 'Enter epilogue equations separated by a semicolon', ...
    'HorizontalAlignment', 'left');

handles.equations = uicontrol( ...
    'Parent', handles.equationPanel, ...
    'Tag', 'text7', ...
    'Style', 'edit', ...
    'BackgroundColor', bg_color, ...
    'Units','normalized', ...
    'Position',[0 0 1 1], ...
    'HorizontalAlignment', 'left', ...
    'Max', 3, ...
    'Min', 1);

if exist(['+' M_.fname filesep 'epilogue_dynamic.m'], 'file') == 2
    % If the epilogue file already exists, parse it and set the edit box
    % string equal to the equations in the file
    eqs = regexp(fileread(['+' M_.fname filesep 'epilogue_dynamic.m']), 'from.*', 'match', 'dotexceptnewline');
    eqs = strrep(eqs, 'from simul_begin_date to simul_end_date do ds.', '');
    eqs = strrep(eqs, 'ds.', '');
    eqs = regexprep(eqs, '\s', '');
    eqs = regexprep(eqs, ['\<(' strjoin(M_.endo_names, '|') ')\(t\)'], '$1');
    eqs = regexprep(eqs, '\(t\)=', '=');
    eqs = regexprep(eqs, '\(t(-\d*)\)', '($1)');
    eqs = regexprep(eqs, 'params\((\d*)\)', '${M_.param_names{str2num($1)}}');
    handles.equations.String = sprintf('%s', strjoin(eqs, '\n'));
end

handles.pushbuttonEpilogue = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonEpilogue', ...
    'Style', 'pushbutton', ...
    'Units','normalized', 'Position', [gui_size.space gui_size.bottom gui_size.button_width gui_size.button_height],...
    'String', 'Run', ...
    'Callback', @EpilogueCallback);

    function EpilogueCallback(~, ~)
        if isempty(handles.equations.String)
            gui_tools.show_warning('You must first enter an equation');
            return
        end
        equations_str = sprintf('%s', handles.equations.String');
        equations_str = equations_str(~isspace(equations_str));
        if equations_str(end) ~= ';'
            gui_tools.show_warning('Equations must end with a semicolon');
            return
        end
        equations = strsplit(equations_str, ';');
        equations = equations(~cellfun('isempty', equations));
        if isempty(equations)
            gui_tools.show_warning('No equations found. Did you end each equation with a semilcolon?');
            return
        end
        lhs = strtok(equations, '=');
        rhs = regexp(equations, ['\<(', strjoin([M_.endo_names ; M_.exo_names], '|'),')\>'], 'match');
        max_lag_eqs = regexp(equations, '\(\-(\d*)\)', 'match');
        max_lag = zeros(length(max_lag_eqs), 1);
        for i = 1:length(max_lag)
            if ~isempty(max_lag_eqs{i})
                max_lag(i) = abs(min(cellfun(@str2num, max_lag_eqs{i})));
            end
        end
        equations = regexprep(equations, '(\w*)=', 'ds.$1(t)=');
        equations = regexprep(equations, ['\<(' strjoin([M_.endo_names; M_.exo_names], '|') ')(?!\()\>'], 'ds.$1(t)');
        equations = regexprep(equations, ['\<(' strjoin([M_.endo_names; M_.exo_names], '|') ')\>\(\-'], 'ds.$1(t-');
        equations = regexprep(equations, ['\<(' strjoin(M_.param_names, '|') ')\>'], '${[''params('' num2str(find(not(cellfun(''isempty'',strfind(M_.param_names,$1))))) '')'']}');
        fidw = fopen(['+' M_.fname filesep 'epilogue_dynamic.m'], 'w');
        if fidw < 0
            gui_tools.show_warning(['Couldn''t open +' M_.fname filesep 'epilogue_dynamic.m for writing']);
            return
        end
        fprintf(fidw, 'function ds = epilogue_dynamic(params, ds)\n');
        fprintf(fidw, '%s\n', '% function ds = epilogue_dynamic(params, ds)');
        fprintf(fidw, '%s\n\n', '% Epilogue file generated by Dynare GUI');
        fprintf(fidw, 'simul_end_date = lastdate(ds);\n\n');
        for i = 1:length(equations)
            fprintf(fidw, 'if ~ds.exist(''%s'')\n', lhs{i});
            fprintf(fidw, '    ds = [ds dseries(NaN(ds.nobs,1), ds.firstdate, ''%s'')];\n', lhs{i});
            fprintf(fidw, 'end\n');
            fprintf(fidw, 'try\n');
            fprintf(fidw, '    simul_begin_date = firstobservedperiod(ds{''%s''}) + %d;\n', strjoin(rhs{i}, ''','''), max_lag(i));
            fprintf(fidw, '    from simul_begin_date to simul_end_date do %s;\n\n', equations{i});
            fprintf(fidw, 'catch\n');
            fprintf(fidw, 'end\n');
        end
        fprintf(fidw, 'end\n');
        fclose(fidw);
        fidw = fopen(['+' M_.fname filesep 'epilogue_static.m'], 'w');
        if fidw < 0
            gui_tools.show_warning(['Couldn''t open +' M_.fname filesep 'epilogue_static.m for writing']);
            return
        end
        fprintf(fidw, 'function ds = epilogue_static(params, ds)\n');
        fprintf(fidw, '%s\n', '% function ds = epilogue_static(params, ds)');
        fprintf(fidw, '%s\n\n', '% Epilogue file generated by Dynare GUI');
        equations = regexprep(equations, '.*=', '');
        equations = regexprep(equations, '\(t((-\d)|(+d))?\)', '');
        for i = 1:length(equations)
            fprintf(fidw, 'epilogue_static_tmp_term = %s;\n', equations{i});
            fprintf(fidw, 'if isdseries(epilogue_static_tmp_term)\n');
            fprintf(fidw, '    ds.%s = epilogue_static_tmp_term;\n', lhs{i});
            fprintf(fidw, 'else\n');
            fprintf(fidw, '    ds.%s = dseries(ones(ds.nobs,1)*epilogue_static_tmp_term, ds.firstdate, ''%s'');\n', lhs{i}, lhs{i});
            fprintf(fidw, 'end\n');
        end
        fprintf(fidw, 'end\n');
        fclose(fidw);
    end

handles.pushbuttonClear = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonClear', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space*2+gui_size.button_width gui_size.bottom gui_size.button_width gui_size.button_height],...
    'String', 'Clear', ...
    'Callback', @ClearCallback);

    function ClearCallback(~, ~)
        handles.equations.String = '';
    end

handles.pushbuttonClose = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonClose', ...
    'Style', 'pushbutton', ...
    'Units', 'normalized', ...
    'Position', [gui_size.space*3+gui_size.button_width*2 gui_size.bottom gui_size.button_width gui_size.button_height],...
    'String', 'Close this tab', ...
    'Callback', {@CloseTab,tabId});

    function CloseTab(~, ~, hTab)
        gui_tabs.delete_tab(hTab);
    end
end
