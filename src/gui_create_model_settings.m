function status = gui_create_model_settings()
% function status = gui_create_model_settings()
% creates initial model settings and saves it in model_settings structure
%
% INPUTS
%   none
%
% OUTPUTS
%   status: status indicator - success (1) or error (0)
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2003-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global M_ ex0_ oo_ project_info model_settings

model_settings = struct();

status = 1;
try
    create_var_cell_array();
    create_params_cell_array();
    create_shocks_cell_array();
    model_settings.shocks_corr = M_.Correlation_matrix;
    gui_tools.project_log_entry('Creating model settings', '...');
    project_info.modified = true;
    jsonfile = [M_.fname filesep() 'model' filesep() 'json' filesep() 'modfile.json'];
    if exist(jsonfile, 'file') ~= 2
        error('Could not find %s. Please rerun the .mod file with the json=compute commandline argument', jsonfile);
    end
    model_settings.jsonmodel = loadjson(jsonfile);
catch ME
    status = 0;
    gui_tools.show_error('Error creating model settings', ME, 'basic');
end

setappdata(0, 'model_settings', model_settings);

    function create_var_cell_array()
        model_settings.variables = cell(M_.endo_nbr, 8);
        model_settings.variables(:, 1) = M_.endo_names;
        model_settings.variables(:, 2) = M_.endo_names_tex;
        model_settings.variables(:, 3) = M_.endo_names_long;
        model_settings.variables(:, 4) = {true};
        model_settings.variables(:, 5) = {'All'};
        model_settings.variables(:, 6) = {true};
        if M_.endo_nbr > M_.orig_endo_nbr
            model_settings.variables(M_.orig_endo_nbr+1:M_.endo_nbr, 4) = {false};
            model_settings.variables(M_.orig_endo_nbr+1:M_.endo_nbr, 5) = {'AUX'};
            model_settings.variables(M_.orig_endo_nbr+1:M_.endo_nbr, 6) = {false};
        end
        model_settings.variables(1:M_.orig_endo_nbr, 7) = {''};
        model_settings.variables(:, 8) = num2cell(oo_.steady_state);
    end

    function create_params_cell_array()
        model_settings.params = cell(M_.param_nbr, 10);
        model_settings.params(:, 1) = M_.param_names;
        model_settings.params(:, 2) = M_.param_names_tex;
        model_settings.params(:, 3) = M_.param_names_long;
        model_settings.params(:, 4) = num2cell(M_.params);
        col = 5;
        if project_info.project_model_stochastic
            col = 7;
            model_settings.params(:, 5) = {''}; % estimated value
            model_settings.params(:, 6) = {''}; % STD
        end
        model_settings.params(:, col)   = {true};
        model_settings.params(:, col+1) = {'All'};
        model_settings.params(:, col+2) = {true};
        model_settings.params(:, col+3) = {''};
    end

    function create_shocks_cell_array()
        if project_info.project_model_stochastic
            model_settings.shocks = cell(M_.exo_nbr, 8);
        else
            model_settings.shocks = cell(M_.exo_nbr, 6);
        end
        model_settings.shocks(:, 1) = M_.exo_names;
        model_settings.shocks(:, 2) = M_.exo_names_tex;
        model_settings.shocks(:, 3) = M_.exo_names_long;
        %stderr for stochastic case or initval for deterministic case - read values from dynare structures
        if project_info.project_model_stochastic
            % stochastic model
            model_settings.shocks(:, 4) = num2cell(sqrt(diag(M_.Sigma_e))); % stderror
            model_settings.shocks(:, 5) = {''};                             % estimated value
            model_settings.shocks(:, 6) = {''};                             % STD
            col = 7;
        else
            if ~isempty(ex0_)
                model_settings.shocks(:, 4) = num2cell(ex0_);
            else
                model_settings.shocks(:, 4) = num2cell(oo_.exo_steady_state); %initval
            end
            col = 5;
        end
        
        if isfield(M_, 'shock_groups')
            fn = fieldnames(M_.shock_groups);
            for i = 1:length(fn)
                gn = fieldnames(M_.shock_groups.(fn{i}));
                for j = 1:length(gn)
                    sn = M_.shock_groups.(fn{i}).(gn{j}).shocks;
                    for k = 1:length(sn)
                        idx = find(strcmp(model_settings.shocks(:,1), sn{k}));
                        model_settings.shocks{idx, col} = fn{i};
                        model_settings.shocks{idx, col+1} = M_.shock_groups.(fn{i}).(gn{j}).label;
                    end
                end
            end
        end
    end
end
